# PostGIS Clustering Demo

Small demonstration on how to use PostGIS to automatically cluster points and generate GeoJSON. Needs PostGIS 3+

## How to run using `docker-compose`

If you have a properly configured docker installation (see [official docker documentation](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
on how to install) you can use `docker-compose` to start the demo. First check the `.env` file and set the `HOST_PGDATA`
variable to the directory where the database will save its data to. As this path must exist - otherwise you'll get an
error on startup - create an empty directory. You may also change the `POSTGRES_PASSWORD`. If you do so, you'll additionally
need to change the `PG_CONNECT_STRING` variable in `docker-compose.yml`.

Now run

```shell script
docker-compose -f docker-compose.yml up --build
```

On first startup docker will download / build all necessary images (this might take a while). This will start up a 
PostgreSQL 9.4 with installed PostGIS 3, the backend node server. The Postgresql server will also bind to localhost:5432.
You can change the local port it binds to by editing `docker-compose.yml` (see https://docs.docker.com/compose/compose-file/#ports)

The Node server binds to port 3000. You should be able to connect to server on http://localhost:3000. You can also change
that port in `docker-compose.yml` e.g. `8080:3000` will bind it http://localhost:8080.

Before the demo can run, you'll need to import the data.

### prepare database
There is a `pg_cluster_demo.sql.zip` which contains a compressed dump of a database. You should be able to import it
with `pg_dump`. You can also create the database by hand and import the data.

Create a new database and enable the PostGIS extension for that database. 

```shell script
createdb -h localhost -U postgres pg-cluster-demo
psql -h localhost -U postgres -d pg-cluster-demo -c "CREATE EXTENSION postgis;"
```

### Import data from KML (needs [GDAL](http://www.sarasafavi.com/installing-gdalogr-on-ubuntu.html))
```shell script
cd data
unzip gsod.kmz
ogrinfo gsod.kml
ogr2ogr -f "PostgreSQL" PG:"host=localhost user=postgres dbname=pg-cluster-demo password=<PASSWORD> port=5432" "gsod.kml" -nln gsod
```

## run without docker
You can run everything without docker. You'll need a local  PostgreSQL with PostGIS 3 installed. Follow the
[official PostGIS documentation](https://postgis.net/install/) to learn how to install for your OS and PostgreSQL version. 

Then follow the steps on how to import the data into the database above.

To run the backend you'll need an installed NodeJS. Follow the [official documentation](https://nodejs.org/en/download/)
on how to install NodeJS on your machine. This demo is tested with version 12. On Linux / MacOS X using
[Node Version Manager (nvm)](https://github.com/nvm-sh/nvm) is highly recommended.

To set the database connection parameters either the an environment variable `PG_CONNECT_STRING` or edit the variable
at the beginning of `server.js`. Then run

```shell script
cd backend
npm install
npm start
```

This should start a node server listening on port 3000. You should be able to connect to it on http://localhost:3000.

## Demo API endpoints

The backend offers all data via a REST API as GeoJSON. The min/max/y/x coordinates are the bounding box to query in LAT/LONG
coordinates. numFeatures is ne number of points / clusters to return.

http://localhost:3000/cluster_polygons

http://localhost:3000/cluster_polygons/:numFeatures

http://localhost:3000/cluster_polygons/:minx/:maxx/:miny/:maxy

http://localhost:3000/cluster_polygons/:minx/:maxx/:miny/:maxy/:numFeatures

http://localhost:3000/cluster_centroids/

http://localhost:3000/cluster_centroids/:numFeatures

http://localhost:3000/cluster_centroids/:minx/:maxx/:miny/:maxy

http://localhost:3000/cluster_centroids/:minx/:maxx/:miny/:maxy/:numFeatures

http://localhost:3000/points/

http://localhost:3000/points/:numFeatures

http://localhost:3000/points/:minx/:maxx/:miny/:maxy

http://localhost:3000/points/:minx/:maxx/:miny/:maxy/:numFeatures


