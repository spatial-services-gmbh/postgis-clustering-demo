const express = require('express')
const { Pool, Client } = require('pg')

var connectionString = process.env.PG_CONNECT_STRING || 'postgresql://postgres:supersecret@localhost:5432/pg-cluster-demo'

// query to select all points within extent $2, cluster them (k-means) into $1 clusters
// and return a GeoJSON with the convex hull of each cluster
const query_cluster_polygons = `
select json_build_object(
               'type', 'FeatureCollection',
               'features', json_agg(ST_AsGeoJSON(t.*)::json)
           )
from (
         select cluster_no,
                count(cluster_no) as                                          points_count,
                ST_ConvexHull(ST_Collect(wkb_geometry))                       geom
         from ( select ST_ClusterKMeans(wkb_geometry, $1) OVER () as cluster_no,
                         ogc_fid,
                         wkb_geometry
                  from gsod
                  where ST_Within(wkb_geometry, ST_GeographyFromText($2)::geometry)
              ) as clusters
         group by cluster_no
     ) as t(cluster_no, points_count, geom)
`;

// query to select all points within extent $2, cluster them (k-means) into $1 clusters
// and return a GeoJSON with the centroid of each cluster
const query_clusters_centroids = `
select json_build_object(
               'type', 'FeatureCollection',
               'features', json_agg(ST_AsGeoJSON(t.*)::json)
           )
from (
         select cluster_no,
                count(cluster_no) as                                          points_count,
                ST_Centroid(ST_ConvexHull(ST_Collect(clusters.wkb_geometry))) geom
         from ( select ST_ClusterKMeans(wkb_geometry, $1) OVER () as cluster_no,
                         ogc_fid,
                         wkb_geometry
                  from gsod
                  where ST_Within(wkb_geometry, ST_GeographyFromText($2)::geometry)
              ) as clusters
         group by cluster_no
     ) as t(cluster_no, points_count, geom)
`;

// return $1 points of the original dataset within extent of $2
const query_points = `
select json_build_object(
               'type', 'FeatureCollection',
               'features', json_agg(ST_AsGeoJSON(t.*)::json)
           )
from (
    select ogc_fid, name, wkb_geometry
    from gsod
    where ST_Within(wkb_geometry, ST_GeographyFromText($2)::geometry)
    limit $1
) as t(ogc_fid, name, wbk_geometry)
`;

// set default values for every query parameter
setParamDefaults = function(params) {
    if (params.numFeatures === undefined) {
        params.numFeatures = 10;
    }

    if (params.minx === undefined || params.minx < -180) {params.minx = -180;}
    if (params.maxx === undefined || params.maxx > 180) {params.maxx = 180;}
    if (params.miny === undefined || params.miny <= -90) {params.miny = -89.99;}
    if (params.maxy === undefined || params.maxy >= 90) {params.maxy = 89.99;}

    return params;
}

// execute query
executeQuery = function(sqlQuery, params, httpResult) {
    const client = new Client({
        connectionString: connectionString,
    })
    client.connect()

    // create BBOX polygon from query parameters.
    const bboxWKT = `SRID=4326;POLYGON((${params.minx} ${params.maxy}, ${params.maxx} ${params.maxy}, ${params.maxx} ${params.miny}, ${params.minx} ${params.miny}, ${params.minx} ${params.maxy}))`;

    client.query(sqlQuery, [params.numFeatures, bboxWKT], (err, queryresult) => {
        if (err) {
            httpResult.status(500).json(err.stack);
        }
        else {
            httpResult.send(queryresult.rows[0].json_build_object);
        }
        client.end();
    });
}

getClusterPolygons = function(req, res) {
    req.params = setParamDefaults(req.params);
    executeQuery(query_cluster_polygons, req.params, res);
}

getClusterCentroids = function(req, res) {
    req.params = setParamDefaults(req.params);
    executeQuery(query_clusters_centroids, req.params, res);
}

getPoints = function(req, res) {
    req.params = setParamDefaults(req.params);
    executeQuery(query_points, req.params, res);
}

// ==== init expressjs app
const app = express()
const port = 3000

app.param('numFeatures', (req, res, next, value) => {
    console.log('numFeatures =', value);
    if (!Number.isInteger(+value) || +value <= 0) {
        next(new Error('numFeatures must be a positive integer'))
    } else {
        req.params.numFeatures = +value
        next()
    }
})

app.param(['minx', 'maxx', 'miny', 'maxy'], (req, res, next, value) => {
    if (Number.isNaN(+value)) {
        next(new Error('minx, maxx, miny, maxy must be a number'))
    } else {
        next()
    }
})

app.get('/', express.static('static'))
app.get('/cluster_polygons', getClusterPolygons)
app.get('/cluster_polygons/:numFeatures', getClusterPolygons)
app.get('/cluster_polygons/:minx/:maxx/:miny/:maxy', getClusterPolygons)
app.get('/cluster_polygons/:minx/:maxx/:miny/:maxy/:numFeatures', getClusterPolygons)

app.get('/cluster_centroids/', getClusterCentroids)
app.get('/cluster_centroids/:numFeatures', getClusterCentroids)
app.get('/cluster_centroids/:minx/:maxx/:miny/:maxy', getClusterCentroids)
app.get('/cluster_centroids/:minx/:maxx/:miny/:maxy/:numFeatures', getClusterCentroids)

app.get('/points/', getPoints)
app.get('/points/:numFeatures', getPoints)
app.get('/points/:minx/:maxx/:miny/:maxy', getPoints)
app.get('/points/:minx/:maxx/:miny/:maxy/:numFeatures', getPoints)

app.listen(port, () => console.log(`pg_cluster_demo listening at http://localhost:${port}`))
